<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<br>
	<br>
	<caption>Age Calculator</caption>
	<form action="AgeCalculator" method="get">
		<label for="name">Enter Name :</label> <input type="text" id="name"
			name="name" required /> <br /> <label for="dob">Select Birth
			Date:</label> <input type="date" id="dob" name="dob" required> 
			<br><input
			type="submit" value="Calculate Age"> <input type="reset"
			value="Cancel">
	</form>
	<br>
	<br>
	<h2 id="result">
		<%
		if (request.getAttribute("name") != null) {
			out.println(request.getAttribute("name")+" "+request.getAttribute("message"));
		}
		%>
	</h2>

</body>
</html>