package com.cognizant;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class AgeService {

	public String calculateAge(String date) {
		LocalDate end_date = LocalDate.now();

		LocalDate start_date = LocalDate.parse(date);

		Period diff = Period.between(start_date, end_date);

		return Integer.toString(diff.getYears()) + " years, " + Integer.toString(diff.getMonths()) + " months, "
				+ Integer.toString(diff.getDays()) + " days old.";
	}
}